# -*- coding: utf-8 -*-
"""
Created on Fri Feb 23 13:23:14 2024

@author: insua
"""
import pandas as pd
from inline_sql import sql

#%%
lista_sedes_datos = pd.read_csv('TablasOriginales/lista-sedes-datos.csv')
lista_secciones = pd.read_csv('TablasOriginales/lista-secciones.csv')
Archivo_Pbi = pd.read_csv('TablasOriginales/API_NY.GDP.PCAP.CD_DS2_en_csv_v2_184.csv', skiprows = 4)

#%%
# =============================================================================
# LISTA SEDES
# =============================================================================



#%%
# =============================================================================
# LISTA SECCIONES
# =============================================================================





#%%
# =============================================================================
# TABLA PBI
# =============================================================================

paises_en_lista_sedes = sql^"""
SELECT DISTINCT *
FROM lista_sedes_datos
"""

print("Cantidad de paises en lista_sedes_datos:", len(paises_en_lista_sedes))


paises_en_ambas_tablas = sql^"""
SELECT DISTINCT l.pais_iso_3
FROM lista_sedes_datos as l
INNER JOIN Archivo_Pbi as p
ON  l.pais_iso_3 = p."Country Code"
"""

#%%
# Cantidad de paises que aparecen en la tabla sede
# no aparecen en la tabla de PBI.

paises_en_sedes_sin_paises_en_PBI = sql^"""
SELECT DISTINCT pais_iso_3
FROM paises_en_lista_sedes

EXCEPT

SELECT DISTINCT pais_iso_3
FROM paises_en_ambas_tablas
"""

print("Cantidad de paises que aparecen en la tabla sede no aparecen en la tabla de PBI:",
      len(paises_en_sedes_sin_paises_en_PBI))
print("Porcentaje de error:",
      100*len(paises_en_sedes_sin_paises_en_PBI)/len(paises_en_lista_sedes))
#%%
# Cuantos paises que aparecen en las dos tablas
# no tienen valor de PBI en 2022.

paises_ambas_sin_PBI = sql^"""
SELECT DISTINCT l.pais_castellano, l.pais_iso_3, "2022"
FROM lista_sedes_datos as l
INNER JOIN Archivo_Pbi as p
ON  l.pais_iso_3 = p."Country Code"
WHERE "2022" IS NULL
"""

print("Cantidad de paises de ambas tablas que no tienen valor del PBI en 2022:",
      len(paises_ambas_sin_PBI))

print("Porcentaje de error:",
      100*len(paises_ambas_sin_PBI)/len(paises_en_lista_sedes))






