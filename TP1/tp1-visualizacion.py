import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from matplotlib import rcParams, ticker

#%%
Pais = pd.read_csv('TablasLimpias/DataFrame-Pais.csv')
Sede = pd.read_csv('TablasLimpias/DataFrame-Sede.csv')
Seccion = pd.read_csv('TablasLimpias/DataFrame-Secciones.csv')
Redes = pd.read_csv('TablasLimpias/DataFrame-Redes.csv')

#%%
Pais.rename(columns = {'Id_Pais':'IdPais', 'Nombre_Pais':'Nombre', 'Pbi2022':'PBI2022'}, inplace = True)
Seccion.rename(columns = {'Id_Sede':'IdSede'}, inplace = True)

#%%
sede_pais = pd.merge(Sede, Pais)

sede_region = sede_pais[["IdSede", "Region"]]

regiones = sede_region['Region'].unique()
nombres_paises = Pais['Nombre'].unique()

regiones_acort = {'AMÉRICA  DEL  NORTE' : 'AME. NOR.',
                  'EUROPA  OCCIDENTAL' : 'EU. OCC.',
                  'AMÉRICA  DEL  SUR' : 'AME. SUR',
                  'ÁFRICA  SUBSAHARIANA' : 'AFR. SSA',
                  'OCEANÍA' : 'OCEANIA',
                  'ASIA' : 'ASIA',
                  'AMÉRICA  CENTRAL  Y  CARIBE' : 'AME. CEN y CAR',
                  'ÁFRICA  DEL  NORTE  Y  CERCANO  ORIENTE' : 'AFR. NOR y OCC',
                  'EUROPA  CENTRAL  Y  ORIENTAL' : 'EU. CEN y ORI'
    }

#%%

rcParams['font.family'] = 'sans-serif'
rcParams['axes.spines.right'] = True
rcParams['axes.spines.left']  = True
rcParams['axes.spines.top']   = True
rcParams['axes.spines.bottom']   = True

colormap = plt.get_cmap('tab20')
colores_dict = {region: colormap.colors[i] for i, region in enumerate(regiones)}


#%% # 
# =============================================================================
#  Cantidad de sedes por región geográfica. Mostrarlos ordenados de manera
#  decreciente por dicha cantidad.
# =============================================================================

counts = sede_region['Region'].value_counts(ascending=False)

fig, ax = plt.subplots()

ax = sns.barplot(data=counts,
                 palette=colores_dict,
                 edgecolor=(.2,.2,.2),
                 zorder=3)


ax.set_title('Cantidad de Sedes Por Region', fontsize=20)
ax.set_xlabel('', fontsize=12)
ax.set_ylabel('Cantidad de Sedes', fontsize=12)
ax.grid(axis='y',zorder=0)

ax.set_xticks(regiones)
ax.set_xticklabels(regiones_acort.values(), rotation = 30, ha='right')
ax.tick_params(axis='y', which='major', labelsize=10)

fig.tight_layout()
plt.savefig('SedesXRegion.png', dpi=300)

#%%
# =============================================================================
#  Boxplot, por cada región geográfica, del PBI per cápita 2022 de los países
#  donde Argentina tiene una delegación. Mostrar todos los boxplots en una
#  misma figura, ordenados por la mediana de cada región.
# =============================================================================

medianas = pd.DataFrame(columns=['Region','Mediana'])

for region in regiones:
    mediana = sede_pais[sede_pais['Region']==region]['PBI2022'].median()
    medianas.loc[len(medianas)] = [region, mediana]

sede_pais_medianas = pd.merge(sede_pais, medianas)[['Region', 'Mediana']]
sede_pais_medianas = sede_pais_medianas.sort_values('Mediana')


fig, ax = plt.subplots()

ax = sns.boxplot(x="Region", 
                 y="PBI2022",  
                 data=sede_pais,
                 order=sede_pais_medianas['Region'],
                 palette=colores_dict, 
                 )

ax.set_title('PBI 2022 Por Region', fontsize=20)
ax.set_xlabel('')
ax.set_ylabel('PBI 2022 (U$S)', fontsize=12)
ax.grid(axis='y',zorder=0)

ax.set_xticks(regiones)
ax.set_xticklabels(regiones_acort.values(), rotation = 30, ha='right')
ax.yaxis.set_major_formatter(ticker.StrMethodFormatter("{x:,.0f}"));

fig.tight_layout()
plt.savefig('PBIXRegion.png', dpi=300)


#%% 
# =============================================================================
#  Relación entre el PBI per cápita de cada país (año 2022 y para todos los
#  países que se tiene información) y la cantidad de sedes en el exterior que
#  tiene Argentina en esos países
# =============================================================================

sede_pais_outer = pd.merge(Pais, Sede, how='outer')

cantidades = pd.DataFrame(columns=['Nombre','Cantidad'])

for nombre in nombres_paises:
    cant = sede_pais_outer[sede_pais_outer['Nombre']==nombre]['IdSede'].count()
    cantidades.loc[len(cantidades)] = [nombre, cant]

sede_pais_cant = pd.merge(sede_pais_outer, cantidades)
sede_pais_cant = sede_pais_cant[['PBI2022','Cantidad','Region']]


fig, ax = plt.subplots()

sns.scatterplot(data=sede_pais_cant, 
                x='PBI2022',
                y='Cantidad',
                hue='Region',
                s=80,
                palette=colores_dict,
                edgecolor=(.2,.2,.2),
                zorder=3,
                )

handles, labels = ax.get_legend_handles_labels()
nombres = [regiones_acort[label] for label in labels]
ax.legend(handles, nombres, loc='upper right', fontsize=8)


ax.set_title('Cantidad de Sedes Por PBI 2022', fontsize=20)
ax.set_xlabel('PBI 2022 (U$S)', fontsize=12)
ax.set_ylabel('Cantidad de Sedes', fontsize=12)
ax.grid(axis='both',zorder=0)

ax.xaxis.set_major_formatter(ticker.StrMethodFormatter("{x:,.0f}"));
ax.set_xlim(0,120000)

fig.tight_layout()
plt.savefig('SedesXPBI.png', dpi=300)