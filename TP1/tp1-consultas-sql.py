import pandas as pd
from inline_sql import sql

#%%
Pais = pd.read_csv('TablasLimpias/DataFrame-Pais.csv')
Sede = pd.read_csv('TablasLimpias/DataFrame-Sede.csv')
Seccion = pd.read_csv('TablasLimpias/DataFrame-Secciones.csv')
Redes = pd.read_csv('TablasLimpias/DataFrame-Redes.csv')

#%%
Pais.rename(columns = {'Id_Pais':'IdPais', 'Nombre_Pais':'Nombre', 'Pbi2022':'PBI2022'}, inplace = True)
Seccion.rename(columns = {'Id_Sede':'IdSede', 'Descripcion_Seccion':'desc_esp'}, inplace = True)
Redes.rename(columns = {'TIPO_RED':'Tipo', 'ID_SEDE':'IdSede'}, inplace = True)

#%%
# =============================================================================
#  Para cada país informar cantidad de sedes, cantidad de secciones en
#  promedio que poseen sus sedes y el PBI per cápita del país en 2022. El orden
#  del reporte debe respetar la cantidad de sedes (de manera descendente). 
#  En caso de empate, ordenar alfabéticamente por nombre de país.
# =============================================================================

# Secciones Promedio por Pais
secc_prom = sql^"""
SELECT DISTINCT IdPais, AVG(cant_secc) AS "Prom Secciones"
FROM (
      SELECT DISTINCT IdSede, COUNT('desc_esp') AS cant_secc
      FROM Seccion
      GROUP BY IdSede
      )
NATURAL JOIN Sede
GROUP BY IdPais
"""

# Agregamos los paises que no aparecen en tabla Pais???
consulta = sql^"""
SELECT DISTINCT Nombre, Sedes, ROUND("Prom Secciones",0) AS 'Prom Secciones',
    ROUND(PBI2022, 2)
FROM (
      SELECT DISTINCT p1.IdPais, COUNT(IdSede) AS Sedes
      FROM Pais AS p1
      NATURAL JOIN Sede
      GROUP BY IdPais
      ) AS temp
NATURAL JOIN Pais AS p2
NATURAL JOIN secc_prom AS sec
ORDER BY Sedes DESC, Nombre
"""

print(consulta)


#%%
# =============================================================================
#  Reportar agrupando por región geográfica la cantidad de países en que
#  Argentina tiene al menos una sede y el promedio del PBI per cápita 2022
#  de dichos países. Ordenar por el promedio del PBI per Cápita. 
# =============================================================================

consulta = sql^"""
SELECT DISTINCT p2.Region, COUNT(p2.IdPais) AS "Paises Con Sedes",
    ROUND(AVG(PBI2022),2) AS "Prom PBI 2022"
FROM Pais AS p2
GROUP BY Region
ORDER BY "Prom PBI 2022"
"""

print(consulta)


#%%
# =============================================================================
#  Para saber cuál es la vía de comunicación de las sedes en cada país, nos
#  hacemos la siguiente pregunta: ¿Cuán variado es, en cada el país, el tipo de
#  redes sociales que utilizan las sedes? Se espera como respuesta que para
#  cada país se informe la cantidad de tipos de redes distintas utilizadas. 
# =============================================================================

consulta = sql^"""
SELECT DISTINCT Nombre, COUNT(tipo) AS "Tipos de Redes"
FROM (
      SELECT DISTINCT IdPais, tipo
      FROM Sede
      NATURAL JOIN Redes
      )
NATURAL JOIN Pais
GROUP BY Nombre
"""

print(consulta)


#%%
# =============================================================================
#  Confeccionar un reporte con la información de redes sociales, donde se
#  indique para cada caso: el país, la sede, el tipo de red social y url utilizada.
#  Ordenar de manera ascendente por nombre de país, sede, tipo de red y
#  finalmente por url.
# =============================================================================

consulta = sql^"""
SELECT DISTINCT Nombre AS Pais, IdSede AS Sede, tipo AS "Red Social", URL
FROM (
      SELECT DISTINCT IdPais, Redes.*
      FROM Redes
      NATURAL JOIN Sede
      )
NATURAL JOIN Pais
ORDER BY Nombre, Sede, tipo, url
"""

print(consulta)
